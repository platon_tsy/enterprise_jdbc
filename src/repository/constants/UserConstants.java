package repository.constants;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserConstants {

    public static final String TABLE = "User";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String ID = "id";

    public static final List<String> COLS = Stream.of(ID, EMAIL, PASSWORD).collect(Collectors.toList());

    public static final String STMT_FIND_ONE = "SELECT " + COLS.stream().collect(Collectors.joining(","))
            + " FROM " + TABLE + " WHERE " + ID + "=?";

}
